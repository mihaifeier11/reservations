package Repository;

import Domain.HasId;

import java.util.Collection;

/**
 * CRUD operations repository interface
 * @param <ID> - type E must have an attribute of type ID
 * @param <T> - type of entities saved in repository
 */
public interface CrudRepository<ID, T extends HasId<ID>> {
    /**
     *
     * @return all entities
     */
    Collection<T> findAll();

}

