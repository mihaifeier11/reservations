package Domain;

import java.util.Objects;

public class User implements HasId<String>{

    String userId;
    Roles role;
    String projectName;

    public User(String userId, Roles role, String projectName) {
        this.userId = userId;
        this.role = role;
        this.projectName = projectName;
    }

    public String getId() {
        return userId;
    }

    public void setId(String userId) {
        this.userId = userId;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(userId, user.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, role, projectName);
    }

    @Override
    public String toString() {
        return userId +
                "," + role +
                "," + projectName;
    }

}
