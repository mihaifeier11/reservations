package Repository;

import Domain.Roles;
import Domain.Tichet;
import Domain.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@SuppressWarnings("ALL")
public class TichetRepository implements CrudRepository<Integer, Tichet> {
    @Override
    public Collection<Tichet> findAll() {
        String pathname = "src/Repository/tichets.txt";
        File file = new File(pathname);
        List<Tichet> allTichets = new ArrayList<>();

        Scanner sc = null;

        //TODO try/ catch
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Tichet tichet;
        String tempString;
        while (sc.hasNextLine()) {

            tempString = sc.nextLine();
            String[] userData = tempString.split(",");

            //TODO try/ catch
            try {
                Date tempDate =new SimpleDateFormat("dd-MM-yyyy").parse(userData[3]);
                tichet = new Tichet(Integer.parseInt(userData[0]), userData[1], userData[2], tempDate, Double.valueOf(userData[4]));
                allTichets.add(tichet);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        sc.close();
        return allTichets;
    }
}
