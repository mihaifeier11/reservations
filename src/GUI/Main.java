package GUI;

import Domain.Tichet;
import Repository.TichetRepository;
import Repository.UserRepository;
import Service.Service;
import UI.Ui;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
    Service service = new Service();
    Ui ui = new Ui();

    @FXML
    DatePicker oldDatePicker, newDatePicker;

    @FXML
    ComboBox<String> comboBox;

    @FXML
    Button button;

    @FXML
    TableView table;

    @FXML
    TableColumn<Tichet, String> idTableColumn, descriptionTableColumn;

    @FXML
    TableColumn<Tichet, Double> costTableColumn;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("gui.fxml"));
        primaryStage.setTitle("Meniu sef");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

//        idTableColumn.setCellValueFactory(new PropertyValueFactory<>("tichetId"));
//        descriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
//        costTableColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
//        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

//        button.setOnAction(e-> {
//            table.setItems(FXCollections.observableArrayList(service.getGUI(old)));
//        });


        //ui.start();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
