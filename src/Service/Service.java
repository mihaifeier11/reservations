package Service;

import Domain.Roles;
import Domain.Tichet;
import Domain.User;
import Repository.TichetRepository;
import Repository.UserRepository;

import java.util.*;
import java.util.function.BiConsumer;

public class Service {
    UserRepository userRepository = new UserRepository();
    TichetRepository tichetRepository = new TichetRepository();

    public Map<Roles, String> numberOfEach() {
        Map<Roles, Integer> users = new HashMap<>();
        users.put(Roles.projectManager, 0);
        users.put(Roles.developer, 0);
        users.put(Roles.devOps, 0);

        userRepository.findAll().forEach(user -> {
            users.put(user.getRole(), users.get(user.getRole()) + 1);
        });


        long nr = userRepository.findAll().stream().count();

        Map<Roles, String> finalUsers = new HashMap<>();
        Integer.parseInt(String.valueOf(users.get(Roles.projectManager)));

        finalUsers.put(Roles.projectManager, Integer.parseInt(String.valueOf(users.get(Roles.projectManager))) + "/" + nr);
        finalUsers.put(Roles.developer, Integer.parseInt(String.valueOf(users.get(Roles.developer))) + "/" + nr);
        finalUsers.put(Roles.devOps, Integer.parseInt(String.valueOf(users.get(Roles.devOps))) + "/" + nr);


        return finalUsers;

    }

    public Map<String, Double> costMediu(){
        List<Float> cost = new ArrayList<>();
        Map<String, Double> tichets = new HashMap<>();
        Map<String, Double> finalTichets = new HashMap<>();
        Map<String, Integer> tichetNr = new HashMap<>();

        List<User> users = new ArrayList<>();
//        users = (List<User>) userRepository.findAll();
//
        userRepository.findAll().forEach(u -> {
            tichets.put(u.getId(), (double) 0);
            tichetNr.put(u.getId(), 0);
        });
//
//        tichetRepository.findAll().forEach(tichet -> {
//
//        });

        tichetRepository.findAll().forEach(tichet -> {
            //System.out.println(tichet.getOpenedBy());
            tichets.put(tichet.getOpenedBy(), tichets.get(tichet.getOpenedBy()) + tichet.getCost());
            tichetNr.put(tichet.getOpenedBy(), tichetNr.get(tichet.getOpenedBy()) + 1);
        });

        tichets.forEach((k,v) -> {
            tichets.put(k, v/tichetNr.get(k));
        });

        tichets.forEach((k,v) -> {
            if (v > 0) {
                finalTichets.put(k, v);
            }

        });



        return finalTichets;
    }

    public List<Tichet> getGUI(Date oldDate, Date newData, String userId) {
        List<Tichet> tichets = new ArrayList<>();
        tichetRepository.findAll().forEach(tichet -> {
            if (tichet.getOpenedBy().equals(userId) && oldDate.before(tichet.getData()) && newData.after(tichet.getData())) {
                tichets.add(tichet);
            }
        });

        return tichets;
    }



}
