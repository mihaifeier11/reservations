package Repository;

import Domain.Roles;
import Domain.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class UserRepository implements CrudRepository<String, User>{
    @Override
    public Collection<User> findAll() {
        String pathname = "src/Repository/users.txt";
        File file = new File(pathname);
        List<User> allUsers = new ArrayList<>();

        Scanner sc = null;

        //TODO try/ catch
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        User user;
        String tempString;
        while (sc.hasNextLine()) {

            tempString = sc.nextLine();
            String[] userData = tempString.split(",");
            user = new User(userData[0], Roles.valueOf(userData[1]), userData[2]);
            allUsers.add(user);
        }

        sc.close();
        return allUsers;
    }
}
