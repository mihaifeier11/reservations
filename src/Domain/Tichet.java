package Domain;

import java.util.Date;
import java.util.Objects;

public class Tichet implements HasId<Integer>{
    Integer tichetId;
    String description;
    String openedBy;
    Date data;
    Double cost;

    public Tichet(Integer tichetId, String description, String openedBy, Date data, Double cost) {
        this.tichetId = tichetId;
        this.description = description;
        this.openedBy = openedBy;
        this.data = data;
        this.cost = cost;
    }

    public Integer getId() {
        return tichetId;
    }

    public void setId(Integer tichedId) {
        this.tichetId = tichedId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOpenedBy() {
        return openedBy;
    }

    public void setOpenedBy(String openedBy) {
        this.openedBy = openedBy;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tichet tichet = (Tichet) o;
        return Objects.equals(tichetId, tichet.tichetId) &&
                Objects.equals(description, tichet.description) &&
                Objects.equals(openedBy, tichet.openedBy) &&
                Objects.equals(data, tichet.data) &&
                Objects.equals(cost, tichet.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tichetId, description, openedBy, data, cost);
    }

    @Override
    public String toString() {
        return tichetId +
                "," + description +
                "," + openedBy +
                "," + data +
                "," + cost;
    }
}
