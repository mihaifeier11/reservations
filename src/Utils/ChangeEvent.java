package Utils;

public class ChangeEvent implements Event{
    private ChangeEventType type;
    private Object data, oldData;

    public ChangeEvent(ChangeEventType type, Object data) {
        this.type = type;
        this.data = data;
    }

    public ChangeEvent(ChangeEventType type, Object data, Object oldData) {
        this.type = type;
        this.data = data;
        this.oldData=oldData;
    }

    public ChangeEventType getType() {
        return type;
    }

    public Object getData() {
        return data;
    }

    public Object getOldData() {
        return oldData;
    }
}
